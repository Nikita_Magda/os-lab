#pragma once
#include "stdlib.h"
#include "vector"
#include "random"
using namespace std;

class PrimeNumber
{
public:
	PrimeNumber();
	~PrimeNumber();

	unsigned long long PrimeNumber::getPrime();
	bool primeMillerRabin(unsigned long num);
	unsigned long QuickPowMod(unsigned long base, unsigned long digitPow, unsigned long mod);
	vector<bool> convertToBinary(unsigned long long num);

private:
	default_random_engine generator;


};

