#include "stdafx.h"
#include "PrimeNumber.h"


PrimeNumber::PrimeNumber()
{
}


PrimeNumber::~PrimeNumber()
{
}

vector<bool> PrimeNumber::convertToBinary(unsigned long long num)
{
	vector<bool> res;
	while (num != 0)
	{
		res.push_back(num % 2 == 1);
		num /= 2;
	}
	return res;
}

unsigned long PrimeNumber::QuickPowMod(unsigned long base, unsigned long digitPow, unsigned long mod)
{
	unsigned long long res = base;
	vector<bool> binaryPow = convertToBinary(digitPow);
	bool temp;
	for (int i = binaryPow.size() - 2; i >= 0; i--)
	{
		if (binaryPow[i])
			res = (((res * res) % mod) * base) % mod;
		else
			res = (res * res) % mod;
	}
	return res;
}

bool PrimeNumber::primeMillerRabin(unsigned long num)
{
	if (num == 1 ||
		num == 2 ||
		num == 3 ||
		num == 5)
		return true;
	if (num % 2 == 0 ||
		num % 3 == 0 ||
		num % 5 == 0)
		return false;
	int s = 0;
	unsigned long t = num - 1;
	while (t % 2 == 0)
	{
		s++;
		t /= 2;
	}
	uniform_int_distribution<unsigned long> distribution(2, num - 2);
	size_t raund = (size_t)log2(num);
	unsigned long temp;
	//loop A:
	for (size_t i = 0; i < raund; i++)
	{
		unsigned long primeEvidence = distribution(generator);
		temp = QuickPowMod(primeEvidence, t, num);
		if (temp == 1 || temp == num - 1)
			continue;
		//Loop B:
		int k = 0;
		for (; k < s - 1; k++)
		{
			temp = (temp * temp) % num;
			if (temp == 1)
				return false;
			else if (temp == num - 1)
			{
				break;
			}
		}
		if (k = s - 1)
			return false;
	}
	return true;
}


unsigned long long PrimeNumber::getPrime()
{
	uniform_int_distribution<unsigned long> distribution(2, 55102); //(55 108)^2 = 3 037 000 499;
																	// (3 037 000 499)^2 = 9 223 372 036 854 775 807
	unsigned long res;
	while (true)
	{
		res = distribution(generator);
		if (primeMillerRabin(res))
		{
			return res;
		}

	}

}