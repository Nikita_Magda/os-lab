#include <Windows.h>
#pragma once

class MailBox
{
public:
	MailBox();
	~MailBox();
	
	const unsigned int PrimeMod = 1000007;

	TCHAR* currentDirectory;
	TCHAR* mailBoxExtention;
	HANDLE currentMailBox;
	unsigned int messagesCount;
	unsigned int messagesSize;
	unsigned int mailBoxMaxSize;

	bool CreateMailBoxDirectory(TCHAR* path);
	bool CreateMailBox(TCHAR* path);
	int OpenMailBox(TCHAR* path);
	bool RemoveMailBox(TCHAR* path);

	int GetMailBoxCount();
	int GetMailCount();
	bool WriteMessage(TCHAR message[]);
	TCHAR* ReadMailByIndex(int idx);
	bool RemoveMailByIndex(int idx);
	bool RemoveAllMails();
	
	void WriteCRC(unsigned int crc, HANDLE mailBox);
	unsigned int CreateCRC(unsigned int fileLength, HANDLE mailBox);
	bool CheckCRC(unsigned int fileLength, HANDLE mailBox);

private:
	void SetFilePointerToCurrentMessage(int msgIndex);
	TCHAR * GetFullPath(TCHAR * path);
	void WriteHeaders();
};

