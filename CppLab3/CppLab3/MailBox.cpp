#include "stdafx.h"
#include "MailBox.h"

MailBox::MailBox()
{
	currentDirectory = _T("D:/LEARNING/II_Course/Term 2/OS/Labs/CppLab3/CppLab3/MailBoxes/");
	mailBoxExtention = _T(".txt");
	currentMailBox = NULL;
	messagesCount = 0;
	messagesSize = 0;
	mailBoxMaxSize = 1048576;
	
}

MailBox::~MailBox()
{
}


bool MailBox::CreateMailBoxDirectory(TCHAR * path)
{
	// while folder is not exist
	while (CreateDirectory(path, NULL) == false)
	{
		// copying folderpath in sTemp
		TCHAR sTemp[MAX_PATH];
		int k = _tcslen(path);
		_tcscpy_s(sTemp, path);

		// creating folder in sTemp path
		while (CreateDirectory(sTemp, NULL) == false)
		{
			// cut path to 1 level higher
			while (sTemp[--k] != '/')
			{
				if (k <= 1)
					return false;
				sTemp[k] = NULL;
			}
		}
	}
	return true;
}

bool MailBox::CreateMailBox(TCHAR * path)
{

	path = GetFullPath(path);
	//Creating mail box file, mod: CREATE_ALWAYS (false if wrong path)
	HANDLE mailBox = CreateFile(
		path,
		(GENERIC_READ | GENERIC_WRITE),
		0,
		NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL
		);
	// if creating success.
	if (mailBox != INVALID_HANDLE_VALUE)
	{
		currentMailBox = mailBox;
		//set current mail box headers
		WriteHeaders();
		WriteCRC(CreateCRC(messagesSize + 12, currentMailBox), currentMailBox);
		return true;
	}
	return false;
}

int MailBox::OpenMailBox(TCHAR * path)
{
	path = GetFullPath(path);
	//Opening mail box file, mod: OPEN_EXISTING (false if not exist)
	HANDLE mailBox = CreateFile(
		path,
		(GENERIC_READ | GENERIC_WRITE),
		0,
		NULL,
		OPEN_EXISTING,
		FILE_ATTRIBUTE_NORMAL,
		NULL
		);
	if (mailBox != INVALID_HANDLE_VALUE)
	{
		//get headers
		unsigned int header[3];
		ReadFile(
			mailBox,
			header,
			sizeof(header),
			0,
			NULL
			);
		//check crc
		if (!CheckCRC(header[1] + 12, mailBox))
		{
			return 2;
		}
		//set current mail box
		currentMailBox = mailBox;
		//set headers
		messagesCount = header[0];
		messagesSize = header[1];
		mailBoxMaxSize = header[2];
		return 0;
	}
	return 1;

}

bool MailBox::RemoveMailBox(TCHAR * path)
{
	path = GetFullPath(path);
	return DeleteFile(path);
}

int MailBox::GetMailBoxCount()
{
	int count = 0;
	HANDLE dir;

	int dirPathLen = _tcslen(currentDirectory) + 1 + 1; // one 1 for \0-symbol
	TCHAR* dirPath = new TCHAR[dirPathLen];
	
	_tcscpy_s(dirPath, dirPathLen, currentDirectory);
	_tcscat_s(dirPath, dirPathLen, _T("*"));

	WIN32_FIND_DATA fileData;
	if ((dir = FindFirstFile(dirPath, &fileData)) == INVALID_HANDLE_VALUE)
	{
		return -1;
	}
	while (FindNextFile(dir, &fileData)) {
		if (!(fileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
			count++;
		}
	}
	return count;

}

int MailBox::GetMailCount()
{
	if (currentMailBox != NULL)
	{
		return messagesCount;
	}
	return -1;
}

bool MailBox::WriteMessage(TCHAR message[])
{
	//size of message
	unsigned int addMessageSize[] = { _tcslen(message) * sizeof(TCHAR) };
	if (12 + messagesSize + 4 + addMessageSize[0] + 4 > mailBoxMaxSize)
	{
		return false;
	}
	//set file pointer to end
	SetFilePointer(
		currentMailBox,
		-4,					//4 bytes takes CRC
		NULL,
		FILE_END
		);
	//write size of message
	bool sizeRes = WriteFile(
		currentMailBox,
		&addMessageSize,
		sizeof(addMessageSize[0]), //4 bytes
		0,
		NULL
		);
	//write message
	bool messRes = WriteFile(
		currentMailBox,
		message,
		addMessageSize[0],
		0,
		NULL
		);
	if (sizeRes && messRes) {
		messagesSize += addMessageSize[0] + sizeof(addMessageSize[0]);
		messagesCount++;
		WriteHeaders();
		WriteCRC(CreateCRC(messagesSize + 12, currentMailBox), currentMailBox);
		return true;
	}
	return false;
}



TCHAR* MailBox::ReadMailByIndex(int idx)
{
	 if (idx < messagesCount)
	 {
		 SetFilePointerToCurrentMessage(idx);
		 unsigned int messageSize[1];
		 //read message length
		 ReadFile(
			 currentMailBox,
			 messageSize,
			 4,
			 0,
			 NULL
			 );
		 int messageLength = messageSize[0] / sizeof(TCHAR);
		 TCHAR* buffer = new TCHAR[messageLength];
		 TCHAR* message = new TCHAR[messageLength + 1];
		 //read message to buffer
		 ReadFile(
			 currentMailBox,
			 buffer,
			 messageSize[0],
			 0,
			 NULL
			 );
		 //get only message (cut other non human-readable symbols)
		 _tcsncpy_s(message, messageLength + 1, buffer, messageLength);
		 return message;
	 }
	 return NULL;
}

bool MailBox::RemoveMailByIndex(int idx)
{
	if (idx < messagesCount) 
	{
		SetFilePointerToCurrentMessage(idx);
		int delMessageSize[1];
		//read message length
		ReadFile(
			currentMailBox,
			delMessageSize,
			4,
			0,
			NULL
			);
		int rightPartLength = messagesSize - delMessageSize[0] - 4;
		TCHAR* buffer = new TCHAR[rightPartLength];
		// move pointer after deleting message
		SetFilePointer(
			currentMailBox,
			delMessageSize[0],
			NULL,
			FILE_CURRENT
			);
		//read message to buffer
		ReadFile(
			currentMailBox,
			buffer,
			rightPartLength,
			0,
			NULL
			);
		//set file pointer to the start of deleting message
		SetFilePointer(
			currentMailBox,
			-(rightPartLength + delMessageSize[0] + 4),
			NULL,
			FILE_CURRENT
			);
		// write over deleting message
		bool writeOver = WriteFile(
			currentMailBox,
			buffer,
			rightPartLength - 1,
			0,
			NULL
			);
		//Truncate file
		bool setEnd = SetEndOfFile(currentMailBox);
		if (writeOver && setEnd) {
			messagesSize -= (delMessageSize[0] + sizeof(delMessageSize[0]));
			messagesCount--;
			WriteHeaders();
			WriteCRC(CreateCRC(messagesSize + 12, currentMailBox), currentMailBox);
			return true;
		}
		return false;
	}
	return false;
}

bool MailBox::RemoveAllMails()
{
	SetFilePointer(
		currentMailBox,
		0,
		NULL,
		FILE_BEGIN
		);
	messagesCount = 0;
	messagesSize = 0;
	WriteHeaders();
	WriteCRC(CreateCRC(messagesSize + 12, currentMailBox), currentMailBox);
	return SetEndOfFile(currentMailBox);
}

bool MailBox::CheckCRC(unsigned int fileLength, HANDLE mailBox)
{
	unsigned int crc = CreateCRC(fileLength, mailBox);
	SetFilePointer(
		mailBox,
		-4,
		NULL,
		FILE_END
		);
	unsigned int prevCrc[1];
	ReadFile(
		mailBox,
		prevCrc,
		4,
		0,
		NULL
		);
	return crc == prevCrc[0];
}

unsigned int MailBox::CreateCRC(unsigned int fileLength, HANDLE mailBox)
{
	unsigned int position = SetFilePointer(
		mailBox,
		0,
		NULL,
		FILE_BEGIN
		);
	unsigned int crc[] = { 0 };
	int temp[1];
	int readLength = 4;
	while (position < fileLength)
	{
		if (position + readLength > fileLength) {
			readLength = fileLength - position;
		}
		ReadFile(
			mailBox,
			temp,
			readLength,
			0,
			NULL
			);
		position += 4;
		crc[0] = (crc[0] + temp[0]) % PrimeMod;
	}
	return crc[0];
}

void MailBox::WriteCRC(unsigned int crc, HANDLE mailBox) {
	unsigned int crcPointer[] = { crc };
	SetFilePointer(
		mailBox,
		0,
		NULL,
		FILE_END
		);
	WriteFile(
		mailBox,
		crcPointer,
		sizeof(crc),
		0,
		NULL
		);
}

TCHAR* MailBox::GetFullPath(TCHAR* path)
{
	if (wcschr(path, L'/') == NULL)
	{
		int len = wcslen(currentDirectory) + wcslen(path) + 4;
		TCHAR* res = new TCHAR[len + 2];
		_tcscpy_s(res, len + 1, currentDirectory);
		_tcscat_s(res, len + 1, path);
		_tcscat_s(res, len + 1, mailBoxExtention);
		return  res;
	}
	return path;

}



void MailBox::SetFilePointerToCurrentMessage(int msgIndex)
{
	//set file pointer after headers
	SetFilePointer(
		currentMailBox,
		12,
		NULL,
		FILE_BEGIN
		);
	int currentMessage = 0;
	unsigned int messageSize[1];
	while (currentMessage != msgIndex)
	{
		//read size of message
		ReadFile(
			currentMailBox,
			messageSize,
			sizeof(messageSize), //4 bytes
			0,
			NULL
			);
		// move pointer on size of message
		SetFilePointer(
			currentMailBox,
			messageSize[0],
			NULL,
			FILE_CURRENT
			);
		currentMessage++;
	}
}

void MailBox::WriteHeaders()
{
	unsigned int header[] = { messagesCount, messagesSize, mailBoxMaxSize };
	// set file pointer to start
	SetFilePointer(
		currentMailBox,
		0,
		NULL,
		FILE_BEGIN
		);
	// write headers (mails count & mails size in bytes)
	WriteFile(
		currentMailBox,
		header,
		sizeof(header), //12 bytes
		0,
		NULL
		);
}