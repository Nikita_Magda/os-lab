// CppLab3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "MailBox.h"
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	MailBox mb = MailBox();	
	_tprintf_s(_T("Mail box One created: %d\r\n"), mb.CreateMailBox(_T("mailBox_One")));
	_tprintf_s(_T("Mail box Three opened: %d\r\n"), mb.OpenMailBox(_T("mailBox_Two")));
	mb.WriteMessage(_T("first mail"));
	mb.WriteMessage(_T("qqq! ������ ���!!!"));
	mb.WriteMessage(_T("Hello world-2? ������ ���-2"));
	mb.WriteMessage(_T("Hello world-2? ������ ���-2"));
	mb.CreateMailBoxDirectory(_T("D:/CRC/test/mailbox"));


	_tprintf_s(_T("You have %d mails\r\n"), mb.GetMailCount());

	mb.RemoveMailByIndex(0);
	unsigned int z = mb.GetMailCount();
	_tprintf_s(_T("You have %d mails\r\n"), mb.GetMailCount());

	TCHAR* t = mb.ReadMailByIndex(0);
	_tprintf_s(_T("Mail number %d: %s\r\n"), 1, mb.ReadMailByIndex(2));

	mb.RemoveMailBox(_T("mailBox_Three"));
	_tprintf_s(_T("You have %d mail boxes\r\n"), mb.GetMailBoxCount());

	//mb.RemoveAllMails();
	_tprintf_s(_T("You have %d mails\r\n"), mb.GetMailCount());


	char x;
	cin >> x;
    return 0;
}

