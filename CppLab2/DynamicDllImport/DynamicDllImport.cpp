// DynamicDllImport.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "random"
#include "iostream"
#include <Windows.h>
using namespace std;

typedef void __stdcall GENERATE_KEYS(unsigned long& openKeyE, unsigned long& privateKeyD, unsigned long long& module);
typedef unsigned long __stdcall ENCODE_NUMBER_RSA(long number, unsigned long openKeyE, unsigned long mod);
typedef long __stdcall DECODE_NUMBER_RSA(unsigned long encodeNunmber, unsigned long privateKeyD, unsigned long mod);

GENERATE_KEYS * generateKeys;
ENCODE_NUMBER_RSA * encodeNumberRSA;
DECODE_NUMBER_RSA * decodeNumberRSA;
TCHAR LibName[] = _T("DLL.dll");

int main()
{
	setlocale(LC_ALL, "rus_rus.866");
	HMODULE resModule = NULL;

	_tprintf(_T("Choose your language: (en - 1/ru - 2)\n"));
	while (resModule == NULL)
	{
		int locale = _getwch();
		if (locale == 49)
		{
			resModule = LoadLibrary(_T("ResourcesEN.dll"));
		}
		else if (locale == 50)
		{
			resModule = LoadLibrary(_T("ResourcesRU.dll"));
		}
	}

	TCHAR s[100];
	LoadString(resModule, 100, s, 100);
	_tprintf(s);

	HMODULE h = LoadLibrary(LibName);
	if (h == NULL)
	{
		_tprintf(_T("Error occured while loading library"));
		return -1;
	}
	generateKeys = (GENERATE_KEYS *)GetProcAddress(h, "generateKeys");
	if (generateKeys == NULL)
	{
		_tprintf(_T("Error occured while loading \"generateKeys\" method"));
		return -1;
	}
	encodeNumberRSA = (ENCODE_NUMBER_RSA *)GetProcAddress(h, "encodeNumberRSA");
	if (encodeNumberRSA == NULL)
	{
		_tprintf(_T("Error occured while loading \"encodeNumberRSA\" method"));
		return -1;
	}
	decodeNumberRSA = (DECODE_NUMBER_RSA *)GetProcAddress(h, "decodeNumberRSA");
	if (decodeNumberRSA == NULL)
	{
		_tprintf(_T("Error occured while loading \"decodeNumberRSA\" method"));
		return -1;
	}

	unsigned long openKeyE, privateKeyD;
	unsigned long long module;
	generateKeys(openKeyE, privateKeyD, module);

	LoadString(resModule, 101, s, 100);
	_tprintf(s);
	printf("%lu\n", privateKeyD);
	LoadString(resModule, 102, s, 100);
	_tprintf(s);
	printf("%lu\n", openKeyE);
	LoadString(resModule, 106, s, 100);
	_tprintf(s);
	printf("%lu\n", module);

	unsigned long long message;
	char buf[110];
	unsigned long encodeMessage;
	long decodeMessage;
	while (true)
	{
		LoadString(resModule, 103, s, 100);
		_tprintf(s);
		gets_s(buf, 100);
		sscanf_s(buf, "%llu", &message);

		char strModule[100];
		_ultoa_s(module, strModule, 100, 10);

		int inputLength = strlen(buf);
		int modLength = strlen(strModule);
		if(inputLength > modLength || message >= module) 
		{
			LoadString(resModule, 107, s, 100);
			_tprintf(L"%s (%lu)\r\n", s, module);
			continue;
		}
		encodeMessage = encodeNumberRSA((unsigned long)message, openKeyE, module);
		decodeMessage = decodeNumberRSA(encodeMessage, privateKeyD, module);
		LoadString(resModule, 104, s, 100);
		_tprintf(s);
		printf("%lu\n", encodeMessage);
		LoadString(resModule, 105, s, 100);
		_tprintf(s);
		printf("%lu\n", decodeMessage);
	}
	FreeLibrary(resModule);
	FreeLibrary(h);
	int x;
	cin >> x;

	FreeLibrary(h);
    return 0;
}

