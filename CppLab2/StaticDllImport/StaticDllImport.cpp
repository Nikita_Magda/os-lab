// StaticDllImport.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "random"
#include "iostream"
#include <Windows.h>
using namespace std;

__declspec(dllimport) void __stdcall generateKeys(unsigned long& openKeyE, unsigned long& privateKeyD, unsigned long long& module);

__declspec(dllimport) unsigned long __stdcall encodeNumberRSA(long number, unsigned long openKeyE, unsigned long mod);

__declspec(dllexport) long __stdcall decodeNumberRSA(unsigned long encodeNunmber, unsigned long privateKeyD, unsigned long mod);

int main()
{
	setlocale(LC_ALL, "rus_rus.866");
	HMODULE resModule;

	_tprintf(_T("Choose your language: (en - 1/ru - 2)\n"));

	int locale = _getwch();
	if (locale == 49)
	{
		resModule = LoadLibrary(_T("ResourcesEN.dll"));
	}
	else if (locale == 50)
	{
		resModule = LoadLibrary(_T("ResourcesRU.dll"));
	}
	TCHAR s[100];
	LoadString(resModule, 100, s, 100);
	_tprintf(s);

	unsigned long openKeyE, privateKeyD;
	unsigned long long module;
	generateKeys(openKeyE, privateKeyD, module);

	LoadString(resModule, 101, s, 100);
	_tprintf(s);
	printf("%lu\n", privateKeyD);
	LoadString(resModule, 102, s, 100);
	_tprintf(s);
	printf("%lu\n", openKeyE);
	LoadString(resModule, 106, s, 100);
	_tprintf(s);
	printf("%lu\n", module);

	unsigned long long message;
	char buf[110];
	unsigned long encodeMessage;
	long decodeMessage;
	while (true)
	{
		LoadString(resModule, 103, s, 100);
		_tprintf(s);
		gets_s(buf, 100);
		sscanf_s(buf, "%llu", &message);

		char strModule[100];
		_ultoa_s(module, strModule, 100, 10);

		int inputLength = strlen(buf);
		int modLength = strlen(strModule);
		if (inputLength > modLength || message >= module)
		{
			LoadString(resModule, 107, s, 100);
			_tprintf(_T("%s (%lu)\r\n"), s, module);
			continue;
		}
		encodeMessage = encodeNumberRSA((unsigned long)message, openKeyE, module);
		decodeMessage = decodeNumberRSA(encodeMessage, privateKeyD, module);
		LoadString(resModule, 104, s, 100);
		_tprintf(s);
		printf("%lu\n", encodeMessage);
		LoadString(resModule, 105, s, 100);
		_tprintf(s);
		printf("%lu\n", decodeMessage);
	}
	FreeLibrary(resModule);
	int x;
	cin >> x;

	return 0;
}

