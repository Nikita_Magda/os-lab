//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ResourcesEN.rc
//
#define LoadComplete                    100
#define PrivateKey                      101
#define PublicKey                       102
#define EnterText                       103
#define EncodedText                     104
#define DecodedText                     105
#define Module                          106
#define InputLimit                      107

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
